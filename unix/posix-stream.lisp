;;
;; unix/posix-stream.lisp - Unix version os-streams.
;;

(in-package :opsys-unix)

(defclass posix-stream (os-stream)
  ()
  (:documentation "A stream using a POSIX file descriptor."))

;; common methods

(defmethod-quiet close ((stream posix-stream) &key abort)
  (declare (ignore abort))
  (syscall (posix-close (os-stream-handle stream))))

(defmethod-quiet close ((stream posix-input-stream) &key abort)
  (declare (ignore abort))
  (when (not (cffi:null-pointer-p (os-stream-input-buffer stream)))
    (cffi:foreign-free (os-stream-input-buffer stream))))

(defmethod-quiet close ((stream posix-output-stream) &key abort)
  (declare (ignore abort))
  (when (not (cffi:null-pointer-p (os-stream-output-buffer stream)))
    (cffi:foreign-free (os-stream-output-buffer stream))))

;; This could in theory be done on a character stream if the ecoding was
;; fixed width.
(defmethod stream-file-position ((stream posix-binary-stream)
				 &optional position-spec)
  (if position-spec
      (progn
	(syscall
	 (posix-lseek (os-stream-handle stream) position-spec +SEEK-SET+))
	;; @@@ Supposedly if it can't do it, we won't get here. But are there
	;; errors where we would want to return NIL. ESPIPE perhaps?
	t)
      (syscall
       (posix-lseek (os-stream-handle stream) 0 +SEEK-CUR+))))

;; output stream methods

(defmethod stream-clear-output ((stream terminal-ansi-stream))
  (clear-output (terminal-output-stream stream)))

(defmethod stream-finish-output ((stream terminal-ansi-stream))
  (terminal-finish-output stream))

(defmethod stream-force-output ((stream terminal-ansi-stream))
  (terminal-finish-output stream)
  (force-output (terminal-output-stream stream)))

(defmethod stream-write-sequence ((stream terminal-ansi-stream) seq start end
				  &key &allow-other-keys)
  (etypecase seq
    (string
     (terminal-write-string stream seq :start start :end end))
    (list
     (with-slots (output-stream) stream
       (loop :with i = 0 :and l = seq
	  :while (and l (< i end))
	  :do
	    (when (>= i start)
	      (write-char (car l) output-stream)
	      (update-column stream (car l)))
	    (setf l (cdr l))
	    (incf i))))))

;; character output stream methods

;; (defmethod stream-advance-to-column ((stream terminal-ansi) column)
;;   ;; @@@
;;   t)

(defmethod stream-line-column ((stream terminal-ansi-stream))
  (terminal-ansi-stream-fake-column stream))

(defmethod stream-start-line-p ((stream terminal-ansi-stream))
  (zerop (stream-line-column stream)))

(defmethod stream-advance-to-column ((stream terminal-ansi-stream) column)
  (write-sequence *endless-spaces*
		  (terminal-output-stream stream)
		  :start 0
		  :end (- column (stream-line-column stream)))
  t)

;;(defmethod stream-fresh-line ((stream terminal-ansi-stream))

;; #+sbcl (defmethod sb-gray:stream-line-length ((stream terminal-ansi-stream))
;;   )

(defmethod stream-write-char ((stream terminal-ansi-stream) char
			     #| &optional start end |#)
  (terminal-write-char stream char))

(defmethod stream-write-string ((stream terminal-ansi-stream) string
			       &optional start end)
  (terminal-write-string stream string :start start :end end))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; stream methods for terminal-ansi, which is also an input stream.

(defmethod stream-clear-input ((stream terminal-ansi))
  (with-slots (typeahead typeahead-pos output-stream) stream
    (setf typeahead nil
	  typeahead-pos nil)
    (clear-input output-stream)))

(defmethod stream-read-sequence ((stream terminal-ansi) seq start end
				 &key &allow-other-keys
					#| &optional (start 0) end |#)
  (declare (ignore stream seq start end))
  nil)

;;(defgeneric stream-peek-char ((stream terminal-ansi))
  ;; This is used to implement ‘peek-char’; this corresponds to
  ;; ‘peek-type’ of ‘nil’.  It returns either a character or ‘:eof’.
  ;; The default method calls ‘stream-read-char’ and
  ;; ‘stream-unread-char’.
;; )

(defmethod stream-read-char-no-hang ((stream terminal-ansi))
  ;; This is used to implement ‘read-char-no-hang’.  It returns either a
  ;; character, or ‘nil’ if no input is currently available, or ‘:eof’
  ;; if end-of-file is reached.  The default method provided by
  ;; ‘fundamental-character-input-stream’ simply calls
  ;; ‘stream-read-char’; this is sufficient for file streams, but
  ;; interactive streams should define their own method.
  (get-char stream :timeout 0))

(defmethod stream-read-char ((stream terminal-ansi))
  (terminal-get-char stream))

(defmethod stream-read-line ((stream terminal-ansi))
  ;; This is used by ‘read-line’.  A string is returned as the first
  ;; value.  The second value is true if the string was terminated by
  ;; end-of-file instead of the end of a line.  The default method uses
  ;; repeated calls to ‘stream-read-char’.
  (multiple-value-bind (result got-eof)
      (read-until (terminal-file-descriptor stream) #\newline)
    (values (or result "")
	    got-eof)))

(defmethod stream-listen ((stream terminal-ansi))
  ;; This is used by ‘listen’.  It returns true or false.  The default
  ;; method uses ‘stream-read-char-no-hang’ and ‘stream-unread-char’.
  ;; Most streams should define their own method since it will usually
  ;; be trivial and will always be more efficient than the default
  ;; method.
  (with-slots (typeahead output-stream) stream
    (or typeahead
	(terminal-listen-for stream 0))))

(defmethod stream-unread-char ((stream terminal-ansi) character)
  ;; Undo the last call to ‘stream-read-char’, as in ‘unread-char’.
  ;; Return ‘nil’.  Every subclass of
  ;; ‘fundamental-character-input-stream’ must define a method for this
  ;; function.
  (add-typeahead stream character))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; End
